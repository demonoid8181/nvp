/* eslint-disable no-new */
import Vue from 'vue'
import App from './client.vue'
import router from './router'
import iView from 'iview'
import './client.less'

Vue.use(iView)

new Vue({
	el: '#app',
	router,
	render: h => h(App)
})
