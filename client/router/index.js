import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
	routes: [
		{
			name: 'maim',
			path: '/',
			component: () => import('../pages/main/main.vue')
		}
	]
})
