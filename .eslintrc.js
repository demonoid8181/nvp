module.exports = {
    parserOptions: {
        parser: 'babel-eslint'
    },
    rules: {
    	indent: [2, "tab"],
		"no-tabs": 0,
		"vue/html-self-closing": ["error", {
			"html": {
				"void": "never",
				"normal": "always",
				"component": "always"
			},
			"svg": "always",
			"math": "always"
		}]
    },
    extends: [
        'plugin:vue/essential',
        'standard'
    ],
    plugins: [
        'vue'
    ]
}
