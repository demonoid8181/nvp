'use strict'
const path = require('path')
const webpack = require('webpack')
const { VueLoaderPlugin } = require('vue-loader')
const HtmlWebpackPlugin = require('html-webpack-plugin')

function resolve (dir) {
	return path.join(__dirname, '..', dir)
}

module.exports = {
	mode: 'development',
	entry: [
		'./client/client.js'
	],
	devServer: {
		hot: true,
		watchOptions: {
			poll: true
		}
	},
	resolve: {
		extensions: ['.js', '.vue', '.json'],
		alias: {
			'vue$': 'vue/dist/vue.esm.js',
			'@': resolve('client')
		}
	},
	module: {
		rules: [
			// {
			// 	test: /\.(js|vue)$/,
			// 	use: 'eslint-loader',
			// 	enforce: 'pre'
			// },
			{
				test: /\.vue$/,
				use: 'vue-loader'
			},
			{
				test: /\.js$/,
				use: 'babel-loader'
			},
			{
				test: /\.css$/,
				use: [
					'vue-style-loader',
					'css-loader'
				]
			},
			{
				test: /\.styl(us)?$/,
				use: [ 'vue-style-loader', 'css-loader', 'stylus-loader' ]
			},
			{
				test: /\.less$/,
				use: [{
					loader: 'vue-style-loader'
				}, {
					loader: 'css-loader'
				}, {
					loader: 'less-loader',
					options: {
						javascriptEnabled: true
					}
				}]
			},
			{
				test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
				loader: 'url-loader',
				options: {
					limit: 10000,
					name: ('image/[name].[hash:7].[ext]')
				}
			},
			{
				test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
				loader: 'url-loader',
				options: {
					limit: 10000,
					name: ('fonts/[name].[hash:7].[ext]')
				}
			}]
	},
	plugins: [
		new webpack.HotModuleReplacementPlugin(),
		new VueLoaderPlugin(),
		new HtmlWebpackPlugin({
			filename: 'index.html',
			template: 'index.html',
			inject: true
		})
	]
}
